import dao.DAO;
import models.Phone;
import models.Laptop;
import models.User;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.Collection;
import static org.junit.jupiter.api.Assertions.*;

public class Tests {
    private  DAO dao;
    public Tests() throws SQLException {
        dao = new DAO(DriverManager.getConnection("jdbc:postgresql://localhost:5432/lab2", "postgres", "pogorelov"));
    }

    @Test
    public void getUserById_id_User() throws SQLException {
        // arrange
        User expected = new User(0, "Johny", 12, "young and perspective");
        //        // act
        User actual = dao.getUserById(0);
        // assert
        assertTrue(expected.equals(actual));
    }
    @Test
    public void getPhoneById_id_Phone() throws SQLException {
        // arrange
        Phone expected = new Phone(2, "iPhone 5s", 200, 1450, 330, "Vodafone");
        // act
        Phone actual = dao.getPhoneById(2);
        // assert
        assertTrue(expected.equals(actual));
    }
    @Test
    public void getLaptopById_id_Laptop() throws SQLException {
        // arrange
        Laptop expected = new Laptop(3, "MacBook Air", 1000, 12000, 1250, 2);
        // act
        Laptop actual = dao.getLaptopById(3);
        // assert
        assertTrue(expected.equals(actual));
    }
    @Test
    public void getUsers_UsersCollection() throws SQLException {
        // arrange
        User[] expected = {
                new User(0, "Johny", 12, "young and perspective"),
                new User(1, "Oleg", 43, "piano player")
        };
        // act
        Collection<User> actual = dao.getUsers();
        User[] actualArr = actual.toArray(new User[actual.size()]);
        // assert
        for(int i = 0; i < expected.length; i++) {
            assertTrue(expected[i].equals(actualArr[i]));
        }

    }
    @Test
    public void getPhones_PhonesCollection() throws SQLException {
        // arrange
        Phone[] expected = {
                new Phone(0, "iPhone 6", 250, 1750, 340, "AT&T"),
                new Phone(1, "xiaomi mi 9", 450, 3500, 420, "Verizon"),
                new Phone(2, "iPhone 5s", 200, 1450, 330, "Vodafone")
        };
        // act
        Collection<Phone> actual = dao.getPhones();
        Phone[] actualArr = actual.toArray(new Phone[actual.size()]);
        // assert
        for(int i = 0; i < expected.length; i++) {
            assertTrue(expected[i].equals(actualArr[i]));
        }
    }
    @Test
    public void getLaptops_LaptopsCollection() throws SQLException {
        // arrange
        Laptop[] expected = {
                new Laptop(3, "MacBook Air", 1000, 12000, 1250, 2)        };
        // act
        Collection<Laptop> actual = dao.getLaptops();
        Laptop[] actualArr = actual.toArray(new Laptop[actual.size()]);
        // assert
        for(int i = 0; i < expected.length; i++) {
            assertTrue(expected[i].equals(actualArr[i]));
        }
    }
}
