package dao;

import models.Laptop;
import models.Phone;
import models.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

public class DAO {

    private DAOImplementation<User> userDAO;
    private DAOImplementation<Laptop> laptopDAO;
    private DAOImplementation<Phone> phoneDAO;

    public DAO(Connection connection) {
        userDAO = new DAOImplementation<User>(User.class, connection);
        laptopDAO = new DAOImplementation<Laptop>(Laptop.class, connection);
        phoneDAO = new DAOImplementation<Phone>(Phone.class, connection);
    }

    public User getUserById(long id) throws SQLException {
        return userDAO.getEntity(id);
    }

    public Laptop getLaptopById(long id) throws SQLException {
        return laptopDAO.getEntity(id);
    }

    public Phone getPhoneById(long id) throws SQLException {
        return phoneDAO.getEntity(id);
    }

    public Collection<User> getUsers() throws SQLException {
        return userDAO.getEntityList();
    }

    public Collection<Laptop> getLaptops() throws SQLException {
        return laptopDAO.getEntityList();
    }

    public Collection<Phone> getPhones() throws SQLException {
        return phoneDAO.getEntityList();
    }
}
