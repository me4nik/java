package dao;

import java.sql.SQLException;
import java.util.List;

public interface IDAOImplementation<T> {
    T getEntity(long id) throws SQLException;
    List<T> getEntityList()  throws SQLException;
}
