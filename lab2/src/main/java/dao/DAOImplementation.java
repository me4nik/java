package dao;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import models.*;


public class DAOImplementation<T> implements IDAOImplementation<T> {
    private Class<T> instance;
    private Connection connection;
    private DiscriminationColumn columnAnnotation;
    private String discriminationColumn;
    private DiscriminatorValue valueAnnotation;
    private String discriminatorValue;

    public DAOImplementation(Class<T> instance, Connection connection){
        this.instance = instance;
        this.connection = connection;
        columnAnnotation = instance.getAnnotation(DiscriminationColumn.class);
        discriminationColumn = columnAnnotation != null ? columnAnnotation.name() : null;
        valueAnnotation = instance.getAnnotation(DiscriminatorValue.class);
        discriminatorValue = valueAnnotation != null ? valueAnnotation.value() : null;
    }

    public T getEntity(long id) throws SQLException{
        String tableName = instance.getAnnotation(TableName.class).name();

        String query = String.format("SELECT * FROM %s WHERE id = %d", tableName, id);
        if (discriminationColumn != null) {
            query = query.concat(String.format(" AND %s = \'%s\'", discriminationColumn, discriminatorValue));
        }
        Statement st = connection.createStatement();
        ResultSet rs = st.executeQuery(query);
        T entity = rs.next() ? createEntity(rs) : null;

        rs.close();
        st.close();
        return entity;
    }
    public List<T> getEntityList() throws SQLException{
        Statement st = connection.createStatement();
        ResultSet resultSet = st.executeQuery("select * from " + instance.getAnnotation(TableName.class).name());

        List<T> entities = new ArrayList<>();
        while(resultSet.next()) {
            if (discriminationColumn == null || discriminatorValue.equals(resultSet.getString(discriminationColumn))){
                entities.add(createEntity(resultSet));
            }
        }
        resultSet.close();
        st.close();
        return entities;
    }

    public T createEntity(ResultSet rs){
        T entity;
        try {
            entity = instance.getConstructor().newInstance();

            List<Field> fields = new ArrayList<>();
            if (instance.getSuperclass() != null){
                fields.addAll(Arrays.asList(instance.getSuperclass().getDeclaredFields()));
            }
            fields.addAll(Arrays.asList(instance.getDeclaredFields()));

            for(Field f: fields){
                f.setAccessible(true);
                try{
                    Object value = rs.getObject(f.getName());
                    f.set(entity, value);
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }
        } catch (IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException
                | InstantiationException e){
            e.printStackTrace();
            entity = null;
        }
        return entity;
    }


}
