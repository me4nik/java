package models;

@TableName(name = "devices")
@DiscriminatorValue(value = "laptop")
public class Laptop extends Device {

    private int numberOfUsbSockets;

    public Laptop(){

    }

    public Laptop(int id, String name, double price, int batteryCapacity, double weight, int numberOfUsbSockets) {
        super(id, name, price, batteryCapacity, weight);
        this.numberOfUsbSockets = numberOfUsbSockets;
    }

    public int getNumberOfUsbSockets() {
        return numberOfUsbSockets;
    }

    public void setNumberOfUsbSockets(int numberOfUsbSockets) {
        this.numberOfUsbSockets = numberOfUsbSockets;
    }

    public boolean equals(Laptop object) {
        return super.equals(object) && this.numberOfUsbSockets == object.getNumberOfUsbSockets();
    }
}