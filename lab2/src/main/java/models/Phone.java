package models;

@TableName(name = "devices")
@DiscriminatorValue(value = "phone")
public class Phone extends Device {

    private String operator;

    public Phone(){

    }

    public Phone(int id, String name, double price, int batteryCapacity, double weight, String operator) {
        super(id, name, price, batteryCapacity, weight);
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public boolean equals(Phone object) {
        return super.equals(object) && this.operator.equals(object.getOperator());
    }
}