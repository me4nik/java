package models;

@DiscriminationColumn(name = "devicetype")
public abstract class Device {

    private int id;
    private String name;
    private double price;
    private int batteryCapacity;
    private double weight;

    protected Device() {

    }
    protected Device(int id, String name, double price, int batteryCapacity, double weight) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.batteryCapacity = batteryCapacity;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public int getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    protected boolean equals(Device object) {
        return id == object.getId() && name.equals(object.getName())
                && batteryCapacity == object.getBatteryCapacity() &&
                weight == object.getWeight() && price == object.getPrice();
    }
}
